﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Configuration ;
using System.Threading.Tasks;

namespace SBOAddonProject1
{
    class bdConexion
    {
        public static SqlConnection Conectar()
        {
            //Capturar los datos desde el archivo de configuracion
            string server = ConfigurationSettings.AppSettings["ServidorSQL"];
            string user = ConfigurationSettings.AppSettings["UsuarioSQL"];
            string bd = ConfigurationSettings.AppSettings["database"];
            string pass = ConfigurationSettings.AppSettings["PasswordSQL"];
            //Crear la conexion con los datos proporcionados
            SqlConnection conexion = new SqlConnection("server='" + server + "';database='" + bd + "';uid='" + user + "';password='" + pass + "'"); //Cadena de conexion para las consultas
            conexion.Open();//Se abre la conexion para consumir la BD
            return conexion;
        }
    }
}
