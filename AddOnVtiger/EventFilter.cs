﻿using SAPbouiCOM;
using System;
using Microsoft.VisualBasic;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Configuration;

namespace SBOAddonProject1
{
    class EventFilter
    {
        private SAPbouiCOM.Application SBO_Application;
       
        //Codigo autogenerado

        //**********************************************************
        // declaring an Event filters container object and an
        // event filter object
        //**********************************************************

        public SAPbouiCOM.EventFilters oFilters;

        public SAPbouiCOM.EventFilter oFilter;


        private void SetApplication()
        {
            //Codigo Autogenerado

            // *******************************************************************
            // Use an SboGuiApi object to establish connection
            // with the SAP Business One application and return an
            // initialized appliction object
            // *******************************************************************
            SAPbouiCOM.SboGuiApi SboGuiApi = null;
            string sConnectionString = null;

            SboGuiApi = new SAPbouiCOM.SboGuiApi();

            // by following the steped specified above the following
            // statment should be suficient for either development or run mode
            sConnectionString = "0030002C0030002C00530041005000420044005F00440061007400650076002C0050004C006F006D0056004900490056"; 

            // connect to a running SBO Application
            SboGuiApi.Connect(sConnectionString);

            // get an initialized application object
            SBO_Application = SboGuiApi.GetApplication(-1);

        }


        private void SetFilters()
        {

            // Crear un nuevo objeto de filtros
            oFilters = new SAPbouiCOM.EventFilters();

            //Asignar filtro de los evengtos a registrar para capturar su ejecucion
            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD); //Filtro "Crear Objeto"
            //Añadirlo a los dormularios requeridos
            oFilter.Add(134);//Business Partner
            oFilter.Add(150);//Articulos

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE);//Filtro "Actualizar Objeto"
            oFilter.Add(134);
            oFilter.Add(150);

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_DELETE);//Filtro "Eliminar Objeto"
            oFilter.Add(134);
            oFilter.Add(150);



            // add an event type to the container
            // this method returns an EventFilter object
            //oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_CLICK);

            // assign the form type on which the event would be processed
            //oFilter.AddEx( "139" ); // Orders Form
            //oFilter.AddEx( "142" ); // Purchase Form
            // oFilter.AddEx("134"); // BussinesParthner Form

            //oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);

            // assign the form type on which the event would be processed
            //oFilter.Add( 139 ); // Orders Form
            //oFilter.Add(134); // BussinesParthner Form


            //Asignamos los filtros al add-on
            SBO_Application.SetFilter(oFilters);

        }

        public EventFilter()
        {

            //*************************************************************
            // set SBO_Application with an initialized application object
            //*************************************************************
            SetApplication();

            //*************************************************************
            // set SBO_Application with an initialized EventFilters object
            //*************************************************************
            SetFilters();

            // events handled by SBO_Application_AppEvent
            SBO_Application.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBO_Application_AppEvent);
            // events handled by SBO_Application_MenuEvent
            SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent);
            // events handled by SBO_Application_ItemEvent
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);

            //Manejador de eventos de formularios por dataEvent
            SBO_Application.FormDataEvent += new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(dataEvent);
        }

        //Datos de Business PArtner
        private string cardCode;
        private string Nombre;
        private string Direccion;
        private string ZipCode;
        private string Numero1;
        private string country;
        private string ciudad;
        private string estado;
        private string eMail;
        private string apartadoPostal;


        //Datos de Items
        private string NombreItem;
        private string Activo;
        private string precioUnidad;
        private string Categoria;
        private string Fabricante;
        private string FechaInicioVenta;
        private string FechaterminoVenta;
        private string CodigoBarras;
        private string Stock;
        private string UnidadUso;


        //Metodo que captura los eventos que se realizan en los formularios correspondientes (registrados)
        private void dataEvent(ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true; //Asignar el valor por default (true) para capturar los eventos
            string key = "";
            if (!BusinessObjectInfo.BeforeAction && BusinessObjectInfo.FormTypeEx == "134") //Business partner
            {
                switch (BusinessObjectInfo.EventType)
                {
                    case BoEventTypes.et_FORM_DATA_ADD://Evento "Crear/Registrar" nuevo usuario
                        if (BusinessObjectInfo.ActionSuccess)//En caso de que se halla realizado la operacion con exito
                        {
                            selectBusinessPartner(BusinessObjectInfo.ObjectKey); //Se envia la respuesta con el CardCode
                        }
                        else//En caso de que no se hubiese concretado el registro por algun error
                        {
                            //Insertar codigo de las acciones que se requieran realizar
                        }
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE://Evento "Eliminar" usuario existente
                       

                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE://Evento "Actualizar" (campos) de un usuario existente
                        key = BusinessObjectInfo.ObjectKey; //Obtenemos la respuesta con el CardCode

                        if (BusinessObjectInfo.ActionSuccess)//En caso de que se halla realizado la operacion con exito
                        {
                            selectBusinessPartner(BusinessObjectInfo.ObjectKey);
                        }
                        else//En caso de que no se hubiese concretado la actualizacion del registro 
                        { 
                            //Insertar codigo de las acciones que se requieran realizar
                        }
                        break;
                    default:
                        break;
                }
            }
            else if (!BusinessObjectInfo.BeforeAction && BusinessObjectInfo.FormTypeEx == "150") //Articulos
            {
                switch (BusinessObjectInfo.EventType)
                {
                    case BoEventTypes.et_FORM_DATA_ADD: //Evento "Crear/Registrar" nuevo articulo
                        if (BusinessObjectInfo.ActionSuccess)
                        {
                            XmlDataDocument doc = new XmlDataDocument();
                            doc.LoadXml(BusinessObjectInfo.ObjectKey);
                            string ItemCode = doc.ChildNodes[1].ChildNodes[0].InnerText; //Rescatamos el ItemCode de la respuesta (xml)
                            Items(ItemCode);
                        }
                        
                        break;
                    case BoEventTypes.et_FORM_DATA_DELETE://Evento "Eliminar" articulo existente

                        break;
                    case BoEventTypes.et_FORM_DATA_UPDATE://Evento "Actualizar" articulo existente
                        if (BusinessObjectInfo.ActionSuccess)
                        {
                            XmlDataDocument respuesta = new XmlDataDocument();
                            respuesta.LoadXml(BusinessObjectInfo.ObjectKey);
                            string ItemCodeTemp = respuesta.ChildNodes[1].ChildNodes[0].InnerText; //Rescatamos el ItemCode de la respuesta (xml)
                            Items(ItemCodeTemp);
                        }
                        break;
                    default:
                        break;
                }
            }
        }


        
        private void SBO_Application_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {

            //********************************************************************************
            // the following are the events sent by the application
            // (Ignore aet_ServerTermination)
            // in order to implement your own code upon each of the events
            // place you code instead of the matching message box statement
            //********************************************************************************


            switch (EventType)
            {

                case SAPbouiCOM.BoAppEventTypes.aet_ShutDown:

                    //SBO_Application.MessageBox( "A Shut Down Event has been caught" + Constants.vbNewLine + "Terminating Add On...", 1, "Ok", "", "" ); 

                    //**************************************************************
                    //
                    // Take care of terminating your AddOn application
                    //
                    //**************************************************************

                    System.Environment.Exit(0);



                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged:

                    //SBO_Application.MessageBox( "A Company Change Event has been caught", 1, "Ok", "", "" ); 

                    //**************************************************************
                    // Check the new company name, if your add on was not meant for
                    // the new company terminate your AddOn
                    //    If SBO_Application.Company.Name Is Not "Company1" then
                    //         Close
                    //    End If
                    //**************************************************************

                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged:

                    //SBO_Application.MessageBox( "A Languge Change Event has been caught", 1, "Ok", "", "" ); 

                    //**************************************************************
                    // Check the new language name, if your AddOn's items needs
                    // to be changed, take care of it at this point
                    //
                    //    Select Case SBO_Application.Language
                    //         Case ln_English:
                    //         Case ln_French:
                    //         Case ln_German:
                    //    End Select
                    //**************************************************************

                    break;
            }


        }
        private void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //********************************************************************************
            // in order to activate your own forms instead of SAP Business One system forms
            // process the menu event by your self
            // change BubbleEvent to True so that SAP Business One won't process it
            //********************************************************************************

            if (pVal.BeforeAction == true)
            {

                SBO_Application.SetStatusBarMessage("Menu item: " + pVal.MenuUID + " sent an event BEFORE SAP Business One processes it.", SAPbouiCOM.BoMessageTime.bmt_Long, true);

                // to stop SAP Business One from processing this event
                // unmark the following statement

                // BubbleEvent = True

            }
            else
            {

                SBO_Application.SetStatusBarMessage("Menu item: " + pVal.MenuUID + " sent an event AFTER SAP Business One processes it.", SAPbouiCOM.BoMessageTime.bmt_Long, true);

            }

        }
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {

            BubbleEvent = true;

            //**************************************************************************
            // BubbleEvent sets the behavior of SAP Business One.
            // False means that the application will not continue processing this event
            // True is the default value
            //**************************************************************************


            if (pVal.FormType != 0 & !pVal.Before_Action)
            {
                //************************************************************
                // the message box form type is 0
                // I chose not to deal with events triggered by a message box
                //************************************************************

                switch (pVal.EventType)
                {

                    case SAPbouiCOM.BoEventTypes.et_CLICK:
                        // Specifies Mouse Up on editable item.

                        if (pVal.ItemUID == "1" && pVal.FormMode == 3) //crear
                        {

                        }
                        else if (pVal.ItemUID == "1" && pVal.FormMode == 2)
                        {

                            //SBO_Application.StatusBar.SetText("Se ha actualizado!", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

                        }
                        else if (pVal.ItemUID == "1" && pVal.FormMode == 1)
                        {
                            //SBO_Application.MessageBox("Boton presionado ok");
                            //SBO_Application.StatusBar.SetText("Se ha actualizado!", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

                            SBO_Application.Forms.ActiveForm.Close();
                        }
                        else if (pVal.ItemUID == "1" && pVal.FormMode == 0)
                        {

                            //SBO_Application.MessageBox("Boton presionado buscar");

                        }
                        // SBO_Application.MessageBox( "An et_CLICK has been sent by a form with the unique ID: " + FormUID, 1, "Ok", "", "" ); 

                        break;

                }
            }
        }
        // Using the Data event 


        private void selectBusinessPartner(string uid)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(uid);
                string cardCode = doc.ChildNodes[1].ChildNodes[0].InnerText;
                SqlCommand query = new SqlCommand("SELECT CardName,Phone1,E_mail,country,address,MailZipcod,County,City,ZipCode FROM OCRD WHERE cardCode ='" + cardCode + "'", bdConexion.Conectar());
                SqlDataReader read = query.ExecuteReader();
                while (read.Read())
                {
                    //Rescatar todos los campos necesarios para registrar en Vtiger
                    // cardCode = read["CardCode"].ToString();
                    Nombre = read["CardName"].ToString();
                    Numero1 = read["Phone1"].ToString();
                    eMail = read["E_mail"].ToString();
                    country = read["Country"].ToString();
                    Direccion = read["Address"].ToString();
                    apartadoPostal = read["MailZipcod"].ToString();
                    ciudad = read["County"].ToString();
                    estado = read["City"].ToString();
                    ZipCode = read["ZipCode"].ToString();

                }
                try
                {
                    string rutaBusinessPartners = ConfigurationSettings.AppSettings["RutaBusinessPartners"];
                    using (StreamWriter txt = new StreamWriter(rutaBusinessPartners + "" + cardCode + ".txt"))
                    {
                        txt.WriteLine("CardName:" + Nombre);
                        txt.WriteLine("Numero Telefono:" + Numero1);
                        txt.WriteLine("E-Mail:" + eMail);
                        txt.WriteLine("Ciudad:" + country);
                        txt.WriteLine("Direccion:" + Direccion);
                        txt.WriteLine("ApartadoPostal" + apartadoPostal);
                        txt.WriteLine("Ciudad:" + ciudad);
                        txt.WriteLine("Estado:" + estado);
                        txt.WriteLine("Codigo Postal:" + ZipCode);
                    }
                    SBO_Application.MessageBox("Se ha guardado el archivo txt en la ruta: " + rutaBusinessPartners);
                    SBO_Application.StatusBar.SetText("Se ha guardado el archivo txt en la ruta" + rutaBusinessPartners, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                }
                catch (Exception ex)
                {
                    SBO_Application.MessageBox(ex.Message);
                    SBO_Application.StatusBar.SetText("Hubo un error:"+ ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                }
               
            }
            catch (Exception ex)
            {
                SBO_Application.MessageBox(ex.Message);
                SBO_Application.StatusBar.SetText("Hubo un error:" + ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
            }
         
        }

        
        private void Items(string ItemCode) {

            try
            {
              

                SqlCommand query = new SqlCommand("SELECT ItemCode,ItemName,sellItem,(Select ItmsGrpNam FROM OITB WHERE ItmsGrpCod=Oitm.ItmsGrpCod) as Categoria,(Select FirmName FROM OMRC WHERE firmCode=OITM.firmCode) as Fabricante,validFrom,validTo,codeBars,OnHand,SalUnitMsr,(Select price FROM ITM1 WHERE ItemCode=OITM.ItemCode and priceList='3') as Precio,ItemCode FROM OITM WHERE ItemCode ='" + ItemCode + "'", bdConexion.Conectar());
                SqlDataReader read = query.ExecuteReader();
                while (read.Read())
                {
                    NombreItem = read["ItemName"].ToString();
                    Activo = read["SellItem"].ToString();
                    precioUnidad = read["Precio"].ToString();
                    Categoria = read["Categoria"].ToString();
                    Fabricante = read["Fabricante"].ToString();
                    FechaInicioVenta = read["ValidFrom"].ToString();
                    FechaterminoVenta = read["ValidTo"].ToString();
                    CodigoBarras = read["codeBars"].ToString();
                    Stock = read["OnHand"].ToString();
                    UnidadUso = read["SalUnitMsr"].ToString();
                }
                string rutaItems = ConfigurationSettings.AppSettings["RutaItems"];
                try
                {
                    using (StreamWriter txt = new StreamWriter( rutaItems+ "" + ItemCode + ".txt"))
                    {
                        txt.WriteLine("Nombre Item:" + NombreItem);
                        txt.WriteLine("Activo?:" + Activo);
                        txt.WriteLine("Precio/unidad:" + precioUnidad);
                        txt.WriteLine("Categoria:" + Categoria);
                        txt.WriteLine("FAbricante:" + Fabricante);
                        txt.WriteLine("FechaInicioVenta" + FechaInicioVenta);
                        txt.WriteLine("FechaTerminoVenta:" + FechaterminoVenta);
                        txt.WriteLine("Codigo de Barras:" + CodigoBarras);
                        txt.WriteLine("En Stock:" + Stock);
                        txt.WriteLine("Unidad de uso:" + UnidadUso);
                    }
                    SBO_Application.MessageBox("Se ha guardado el archivo txt en la ruta: " + rutaItems);
                    SBO_Application.StatusBar.SetText("Se ha guardado el archivo txt en la ruta: " + rutaItems, BoMessageTime.bmt_Long, BoStatusBarMessageType.smt_Success);
                }
                catch (Exception ex)
                {
                    SBO_Application.MessageBox(ex.Message);
                    SBO_Application.StatusBar.SetText("Hubo algun error: " + ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                }
            }
            catch (Exception ex)
            {
                SBO_Application.MessageBox(ex.Message);
                SBO_Application.StatusBar.SetText("Hubo algun error: " + ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
            }
        }
    }
}
